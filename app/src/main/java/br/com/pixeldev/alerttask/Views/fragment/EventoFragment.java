package br.com.pixeldev.alerttask.views.fragment;


import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.pixeldev.alerttask.R;
import br.com.pixeldev.alerttask.Utility;
import br.com.pixeldev.alerttask.views.LoginActivity;
import br.com.pixeldev.alerttask.data.SessionHandler;
import br.com.pixeldev.alerttask.database.Cadastro;
import br.com.pixeldev.alerttask.database.Deleta;
import br.com.pixeldev.alerttask.database.Leitura;
import br.com.pixeldev.alerttask.list.TasksAdapter;
import br.com.pixeldev.alerttask.model.Task;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventoFragment extends Fragment {

    private List<Task> myTasks = new ArrayList<>();
    Boolean notificado = false;

    public EventoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_evento, container, false);

        if (Utility.isOnline()) {
            getTask(view);//Se tiver online sincroniza taferas
        }

        showTasks(view);//Mostra tarefas cadastradas

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkTask();
    }

    public void removeTask(final String id) {

        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext())
                .title("Confirmar ação")
                .content("Quer mesmo remover esta tafera?")
                .positiveText("Confirmar")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        //Validação de dados
                        Utility.showLoading("Favor aguarde...", getContext());
                        if (!Utility.isOnline()) {
                            Utility.hideDialog();
                            Utility.showMsg("Ooops!", "Não foi possivel conectar com servidor", getContext());
                        } else {
                            Utility.hideDialog();
                            String uid = SessionHandler.getUid(getContext());
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            database.getReference("tasks").child(uid).child(id).removeValue();
                        }
                    }
                })
                .negativeText("Cancelar");

        MaterialDialog dialog = builder.build();
        dialog.show();


    }

    public void showTasks(View view) {
        Leitura leitura = new Leitura(getContext());
        leitura.ExeLeitura("tasks");

        myTasks.clear();

        try {
            JSONArray jsonArray = leitura.getResultado().getJSONArray("resultado");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject data = jsonArray.getJSONObject(i);

                Task task = new Task(data.getString("data"), data.getString("hora"), data.getString("nome"));
                task.id = data.getString("id");
                myTasks.add(task);
            }

            RecyclerView recycler = view.findViewById(R.id.recycler);
            TasksAdapter adapter = new TasksAdapter(myTasks, this);
            recycler.setAdapter(adapter);
            recycler.setLayoutManager(new LinearLayoutManager(getContext()));
            checkTask();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void checkTask() {

        int tarefas = 0;

        for (int i = 0; i < myTasks.size(); i++) {
            Task task = myTasks.get(i);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();

            if (task.data.equals(dateFormat.format(date)))
                tarefas++;

        }

        if (tarefas > 0 && !notificado) {
            notificado = true; //Marcando como notificado para nao ficar disparando

            Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                v.vibrate(500);
            }

            if (tarefas == 1) {
                Toast.makeText(getContext(), "Você tem " + tarefas + " evento hoje!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Você tem " + tarefas + " eventos hoje!", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void getTask(final View view) {

        ValueEventListener taskListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //Remove todas as tarefas
                Deleta deleta = new Deleta(getContext());
                deleta.ExeDeleta("tasks", "id != '' ");

                for (DataSnapshot uniqueUserSnapshot : dataSnapshot.getChildren()) {
                    Task Task = uniqueUserSnapshot.getValue(Task.class);

                    Cadastro cadastro = new Cadastro(getContext());
                    ContentValues registro = new ContentValues();
                    registro.put("id", uniqueUserSnapshot.getKey());
                    registro.put("data", Task.data);
                    registro.put("hora", Task.hora);
                    registro.put("nome", Task.nome);

                    cadastro.ExeCadastro("tasks", registro);
                }

                showTasks(view);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                showTasks(view);
            }
        };

        String uid = SessionHandler.getUid(getContext());

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("tasks").child(uid);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            myRef.addValueEventListener(taskListener);

        } else {
            Intent intent = new Intent(getContext(), LoginActivity.class);
            startActivity(intent);
            getActivity().finish();
        }
    }

}
