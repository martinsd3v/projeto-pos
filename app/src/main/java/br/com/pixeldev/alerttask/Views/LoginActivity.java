package br.com.pixeldev.alerttask.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import br.com.pixeldev.alerttask.R;
import br.com.pixeldev.alerttask.Utility;
import br.com.pixeldev.alerttask.data.SessionHandler;
import br.com.pixeldev.alerttask.database.Deleta;


public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "TAGSRTS";
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.my_toolbar);
        toolbar.setTitle(getResources().getString(R.string.app_name));
        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();

        //Efetuando logout
        mAuth.signOut();
        //Remove todas as tarefas
        Deleta deleta = new Deleta(this);
        deleta.ExeDeleta("tasks", "id != '' ");
        //Apagando sessao
        SessionHandler.saveUid("", this);

        Button cadastrar = findViewById(R.id.button_cadastrar);
        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(LoginActivity.this, CadastrarActivity.class);
                startActivity(myIntent);
            }
        });

        Button login = findViewById(R.id.button_login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText txt_password = findViewById(R.id.txt_password);
                EditText txt_user = findViewById(R.id.txt_user);

                //Validação de dados
                Utility.showLoading("Favor aguarde...", LoginActivity.this);
                if (!Utility.isOnline()) {
                    Utility.hideDialog();
                    Utility.showMsg("Ooops!", "Não foi possivel conectar com servidor", LoginActivity.this);
                } else if (txt_password.getText().toString().length() < 1 || txt_user.getText().toString().length() < 1) {
                    Utility.hideDialog();
                    Utility.showMsg("Ooops!", "Preencha todos os campos!", LoginActivity.this);
                } else {
                    loginUser(txt_user.getText().toString(), txt_password.getText().toString());
                }

            }
        });

    }

    public void loginUser(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Utility.hideDialog();
                            Toast.makeText(LoginActivity.this, "Dados não conferem!", Toast.LENGTH_SHORT).show();
                        } else {
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            if (user != null) {
                                Utility.hideDialog();

                                String email = user.getEmail();
                                String uid = user.getUid();

                                Log.d(TAG, "email :" + email);
                                Log.d(TAG, "uid :" + uid);

                                SessionHandler.saveEmail(email, LoginActivity.this);
                                SessionHandler.saveUid(uid, LoginActivity.this);

                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();

                            }
                        }
                    }
                });
    }
}
