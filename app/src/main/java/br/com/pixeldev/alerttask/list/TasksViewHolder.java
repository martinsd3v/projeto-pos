package br.com.pixeldev.alerttask.list;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;


import br.com.pixeldev.alerttask.R;
import br.com.pixeldev.alerttask.model.Task;

public class TasksViewHolder extends LinearLayout {
    private Context mContext;
    private Task mLog;

    public TasksViewHolder(Context context) {
        super(context);
        mContext = context;
        setup();
    }

    public TasksViewHolder(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        setup();
    }

    public void setup() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.adapter_evento, this);
    }

    public void setLog(Task log, final int possition) {
        mLog = log;

        TextView evento_nome = (TextView) findViewById(R.id.evento_nome);
        evento_nome.setText(mLog.nome);
        TextView evento_hora = (TextView) findViewById(R.id.evento_hora);
        evento_hora.setText(mLog.data);

    }
}
