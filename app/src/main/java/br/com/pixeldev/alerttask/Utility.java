package br.com.pixeldev.alerttask;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.IOException;

public class Utility {
    static MaterialDialog dialog;

    public static void showLoading(String msg, Context context) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .title(msg)
                .progress(true, 0)
                .progressIndeterminateStyle(true)
                .cancelable(true);

        dialog = builder.build();
        dialog.show();
    }

    public static void showMsg(String title, String msg, Context context) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .title(title)
                .content(msg)
                .positiveText("OK");

        dialog = builder.build();
        dialog.show();
    }

    public static void hideDialog() {
        if (dialog != null)
            dialog.dismiss();
    }


    public static boolean isValidEmail(String target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        }
        catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        return false;
    }
}
