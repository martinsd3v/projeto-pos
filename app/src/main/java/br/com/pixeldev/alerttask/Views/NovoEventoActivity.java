package br.com.pixeldev.alerttask.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.HashMap;

import br.com.pixeldev.alerttask.R;
import br.com.pixeldev.alerttask.Utility;
import br.com.pixeldev.alerttask.data.SessionHandler;

public class NovoEventoActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final String TAG = "TAGSRTS";
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    EditText data;
    EditText hora;
    EditText nome;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evento);

        Toolbar toolbar = findViewById(R.id.my_toolbar);
        toolbar.setTitle(getResources().getString(R.string.event_new));
        setSupportActionBar(toolbar);

        //SETANDO BACK_BUTTON
        toolbar.setNavigationIcon(R.drawable.icon_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        Button cadastrar = findViewById(R.id.button_cadastrar);
        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Validação de dados
                Utility.showLoading("Favor aguarde...", NovoEventoActivity.this);
                if (!Utility.isOnline()) {
                    Utility.hideDialog();
                    Utility.showMsg("Ooops!", "Não foi possivel conectar com servidor", NovoEventoActivity.this);
                } else if (data.getText().toString().length() < 10 || hora.getText().toString().length() < 5 || nome.getText().toString().length() < 1) {
                    Utility.hideDialog();
                    Utility.showMsg("Ooops!", "Informe todos os campos corretamente!", NovoEventoActivity.this);
                } else {
                    newTask(data.getText().toString(), hora.getText().toString(), nome.getText().toString());
                }
            }
        });


        data = findViewById(R.id.txt_data);
        nome = findViewById(R.id.txt_nome);
        data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        NovoEventoActivity.this,
                        now.get(Calendar.YEAR), // Initial year selection
                        now.get(Calendar.MONTH), // Initial month selection
                        now.get(Calendar.DAY_OF_MONTH) // Inital day selection
                );
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        hora = findViewById(R.id.txt_hora);
        hora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        NovoEventoActivity.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false

                );
                tpd.show(getFragmentManager(), "Timepickerdialog");

            }
        });

    }

    public void newTask(final String data, final String hora, String nome) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("tasks");

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            Utility.hideDialog();

            // Name, email address, and profile photo Url
            String uid = SessionHandler.getUid(this);

            HashMap<String, Object> insert = new HashMap<>();
            insert.put("data", data);
            insert.put("hora", hora);
            insert.put("nome", nome);

            myRef.child(uid).push().setValue(insert);

            Intent intent = new Intent(NovoEventoActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();

        } else {
            Intent intent = new Intent(NovoEventoActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String month = (monthOfYear + 1) < 10 ? "0" + (monthOfYear + 1) : String.valueOf(monthOfYear + 1);
        String day = dayOfMonth < 10 ? "0" + dayOfMonth : String.valueOf(dayOfMonth);

        data.setText(day + "/" + month + "/" + year);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hour = hourOfDay < 10 ? "0" + hourOfDay : String.valueOf(hourOfDay);
        String minut = minute < 10 ? "0" + minute : String.valueOf(minute);


        hora.setText(hour + ":" + (minut));
    }
}
