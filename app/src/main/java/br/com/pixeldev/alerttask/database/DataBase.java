package br.com.pixeldev.alerttask.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import br.com.pixeldev.alerttask.Bootstrap;

public class DataBase  extends SQLiteOpenHelper {
    private static String TASKS_RM() {
        String Retorno = "DROP TABLE IF EXISTS tasks";
        return Retorno.trim();
    }

    private static String TASKS_CR() {
        String Retorno = "";
        Retorno += "CREATE TABLE IF NOT EXISTS tasks (";
        Retorno += "   id VARCHAR PRIMARY KEY,";
        Retorno += "   data VARCHAR,";
        Retorno += "   hora VARCHAR,";
        Retorno += "   nome VARCHAR";
        Retorno += ")";
        return Retorno.trim();
    }

    public DataBase (Context context) {
        super(context, Bootstrap.DATABASE_NAME, null, Bootstrap.DATABASE_VERSION);
    }

    public SQLiteDatabase getDb(){
        return this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        System.out.println("CRIOU AS TASKS");
        db.execSQL(DataBase.TASKS_CR());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        System.out.println("REMOVEU AS TASKS");

        db.execSQL(DataBase.TASKS_RM());
        onCreate(db);
    }
}
