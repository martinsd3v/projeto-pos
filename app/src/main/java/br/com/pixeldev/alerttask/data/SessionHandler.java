package br.com.pixeldev.alerttask.data;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionHandler {

    private static final String FOLDER = "SECTION";

    public static void saveEmail(String email, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(FOLDER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("email", email);
        editor.apply();
    }

    public static String getEmail(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(FOLDER, Context.MODE_PRIVATE);
        return sharedPreferences.getString("email", "");
    }

    public static void saveUid(String uid, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(FOLDER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("uid", uid);
        editor.apply();
    }

    public static String getUid(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(FOLDER, Context.MODE_PRIVATE);
        return sharedPreferences.getString("uid", "");
    }
}
