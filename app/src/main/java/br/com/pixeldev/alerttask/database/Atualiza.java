package br.com.pixeldev.alerttask.database;

import android.content.ContentValues;
import android.content.Context;

import org.json.JSONObject;

/**
 * Created by MarceloMartins on 8/5/16.
 */
public class Atualiza {
    private String Tabela = null;
    private String Termos = null;
    private ContentValues Dados = new ContentValues();
    private JSONObject Resultado = new JSONObject();
    private Context mContext = null;

    //Contrutor da classe
    public Atualiza(Context context) {
        mContext = context;
    }

    //Montando a query de atualizaçao
    public void ExeAtualiza(String tabela, ContentValues dados, String termos) {
        this.Tabela = tabela;
        this.Termos = termos;
        this.Dados = dados;
        this.Execute();
    }

    //Obtem a conexao a syntax e executa a query
    private void Execute() {
        DataBase dataBase = new DataBase(mContext);
        try {
            long Return = dataBase.getDb().update(this.Tabela, this.Dados, this.Termos, null);
            Resultado.put("resultado", Return);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Retornando os resultados
    public JSONObject getResultado() {
        return Resultado;
    }
}
