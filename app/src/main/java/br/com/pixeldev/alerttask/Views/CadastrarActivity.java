package br.com.pixeldev.alerttask.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import br.com.pixeldev.alerttask.R;
import br.com.pixeldev.alerttask.Utility;
import br.com.pixeldev.alerttask.data.SessionHandler;

public class CadastrarActivity extends AppCompatActivity {

    private static final String TAG = "TAGSRTS";
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);

        Toolbar toolbar = findViewById(R.id.my_toolbar);
        toolbar.setTitle(getResources().getString(R.string.cadastro_title));
        setSupportActionBar(toolbar);

        //SETANDO BACK_BUTTON
        toolbar.setNavigationIcon(R.drawable.icon_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mAuth = FirebaseAuth.getInstance();

        Button cadastrar = findViewById(R.id.button_cadastrar);
        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txt_password = findViewById(R.id.txt_password);
                EditText txt_nome = findViewById(R.id.txt_nome);
                EditText txt_email = findViewById(R.id.txt_email);

                //Validação de dados
                Utility.showLoading("Favor aguarde...", CadastrarActivity.this);
                if (!Utility.isOnline()) {
                    Utility.hideDialog();
                    Utility.showMsg("Ooops!", "Não foi possivel conectar com servidor", CadastrarActivity.this);
                } else if (txt_password.getText().toString().length() < 1 || txt_nome.getText().toString().length() < 1 || txt_email.getText().toString().length() < 1) {
                    Utility.hideDialog();
                    Utility.showMsg("Ooops!", "Preencha todos os campos!", CadastrarActivity.this);
                } else if (txt_password.getText().toString().length() < 6) {
                    Utility.hideDialog();
                    Utility.showMsg("Ooops!", "A senha deve conter mais de 6 caracteres!", CadastrarActivity.this);
                } else {
                    newUser(txt_nome.getText().toString(), txt_email.getText().toString(), txt_password.getText().toString());
                }
            }
        });

    }

    public void newUser(final String name, final String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Utility.hideDialog();
                            Toast.makeText(CadastrarActivity.this, "Não foi possivel cadastrar!", Toast.LENGTH_SHORT).show();
                        } else {

                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference myRef = database.getReference("users");

                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            if (user != null) {
                                Utility.hideDialog();

                                // Name, email address, and profile photo Url
                                String uid = user.getUid();

                                myRef.child(uid).child("name").setValue(name);
                                myRef.child(uid).child("email").setValue(email);

                                SessionHandler.saveEmail(email, CadastrarActivity.this);
                                SessionHandler.saveUid(uid, CadastrarActivity.this);

                                Intent intent = new Intent(CadastrarActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }

                    }
                });
    }

}
