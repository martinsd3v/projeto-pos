package br.com.pixeldev.alerttask.model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Task {

    public String data;
    public String hora;
    public String nome;
    public String id;

    public Task() {
    }

    public Task(String data, String hora, String nome) {
        this.data = data;
        this.hora = hora;
        this.nome = nome;
    }
}