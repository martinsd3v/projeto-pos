package br.com.pixeldev.alerttask.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import br.com.pixeldev.alerttask.R;
import br.com.pixeldev.alerttask.views.fragment.EventoFragment;
import br.com.pixeldev.alerttask.model.Task;

public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.ViewHolder> {

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView nameTextView;
        TextView horaTextView;
        RelativeLayout deleteButton;
        FrameLayout ultimo;

        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            nameTextView = itemView.findViewById(R.id.evento_nome);
            horaTextView = itemView.findViewById(R.id.evento_hora);
            deleteButton = itemView.findViewById(R.id.evento_deleta);
            ultimo = itemView.findViewById(R.id.ultimo);
        }
    }

    private List<Task> mTasks;
    private EventoFragment mFragment;

    // Pass in the contact array into the constructor
    public TasksAdapter(List<Task> tasks, EventoFragment fragment) {
        mTasks = tasks;
        mFragment = fragment;
    }

    @Override
    public TasksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.adapter_evento, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(TasksAdapter.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        final Task task = mTasks.get(position);

        viewHolder.nameTextView.setText(task.nome);
        viewHolder.horaTextView.setText(task.data+" "+task.hora);
        viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragment.removeTask(task.id);
            }
        });

        if (position + 1 == mTasks.size()) {
            viewHolder.ultimo.setVisibility(View.VISIBLE);
        }
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mTasks.size();
    }
}