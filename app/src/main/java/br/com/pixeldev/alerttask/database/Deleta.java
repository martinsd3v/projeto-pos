package br.com.pixeldev.alerttask.database;

import android.content.Context;

import org.json.JSONObject;

/**
 * Created by MarceloMartins on 8/5/16.
 */
public class Deleta {
    private String Tabela = null;
    private String Termos = null;
    private JSONObject Resultado = new JSONObject();
    private Context mContext = null;

    //Contrutor da classe
    public Deleta(Context context) {
        mContext = context;
    }

    //Montando a query de consulta
    public void ExeDeleta(String tabela, String termos) {
        this.Tabela = tabela;
        this.Termos = termos;
        this.Execute();
    }

    //Obtem a conexao a syntax e executa a query
    private void Execute() {
        DataBase dataBase = new DataBase(mContext);
        try {
            long Return = dataBase.getDb().delete(this.Tabela, this.Termos, null);
            Resultado.put("resultado", Return);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Retornando os resultados
    public JSONObject getResultado() {
        return Resultado;
    }
}
